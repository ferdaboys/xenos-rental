package com.xenos.rental.exception;

import com.stripe.exception.CardException;
import com.xenos.rental.exception.model.ApiError;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ControllerExceptionHandler {

    @ExceptionHandler
    public ResponseEntity handleNotFoundException(DuplicateKeyException ex) {
        return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiError(HttpStatus.CONFLICT, "AUTH.100", "A user with that username already exists"));
    }

    @ExceptionHandler
    public ResponseEntity handleSearchCriteriaInvalidException(InvalidSearchCriteriaException ex) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ApiError(HttpStatus.BAD_REQUEST, "AUTH.101", ex.getMessage()));
    }
    @ExceptionHandler
    public ResponseEntity handleSearchValueInvalidException(InvalidSearchValueException ex) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ApiError(HttpStatus.BAD_REQUEST, "AUTH.102", ex.getMessage()));
    }

    @ExceptionHandler
    public ResponseEntity handleBadCredentialsException(BadCredentialsException ex) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ApiError(HttpStatus.BAD_REQUEST, "AUTH.103", ex.getMessage()));
    }

    @ExceptionHandler
    public ResponseEntity handleUserNotFoundException(UserNotFoundException ex) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ApiError(HttpStatus.NOT_FOUND, "AUTH.104", ex.getMessage()));
    }

    @ExceptionHandler
    public ResponseEntity handleBadCardException(CardException ex) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ApiError(HttpStatus.NOT_FOUND, "PAY.100", ex.getMessage()));
    }

    @ExceptionHandler
    public ResponseEntity handleResetTokenInvalidException(ResetTokenInvalidException ex) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ApiError(HttpStatus.NOT_FOUND, "RESET.100", ex.getMessage()));
    }


}
