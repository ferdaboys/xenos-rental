package com.xenos.rental.controller;

import com.xenos.rental.model.Expense;
import com.xenos.rental.repository.auth.XenosExpenseRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@Slf4j
@RequestMapping("/api")
public class ExpenseController {

    private final XenosExpenseRepository repository;
    public ExpenseController(XenosExpenseRepository repository) {
        this.repository = repository;
    }

    @GetMapping("/expenses/{propertyId}")
    public Flux<Expense> get (@PathVariable String propertyId) {
        log.info("Retrieving expense by property id {0}", propertyId);
        return this.repository.findByPropertyId(propertyId);
    }

    @GetMapping("/expenses/user/{userId}")
    public Flux<Expense> getByUserId (@PathVariable String userId) {
        log.info("Retrieving expense by user id");
        return this.repository.findAllByUserId(userId);
    }


    @PostMapping("/expenses/save")
    public Mono<Expense> save (@RequestBody Expense expense) {
        log.info("Saving Expense");
        return this.repository.save(expense);
    }
}
