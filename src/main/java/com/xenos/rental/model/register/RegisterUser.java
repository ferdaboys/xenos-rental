package com.xenos.rental.model.register;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class RegisterUser {
    private String email;
    private String password;
    private String username;
}
