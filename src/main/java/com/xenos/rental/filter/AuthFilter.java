package com.xenos.rental.filter;

import com.xenos.rental.exception.AuthTokenInvalidException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.web.server.context.WebSessionServerSecurityContextRepository;
import org.springframework.session.data.mongo.MongoSession;
import org.springframework.session.data.mongo.ReactiveMongoOperationsSessionRepository;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.WebFilter;
import org.springframework.web.server.WebFilterChain;
import org.springframework.web.server.WebSession;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Configuration
public class AuthFilter implements WebFilter {

    //                .authorizeExchange().pathMatchers("/api/auth/**").permitAll()
    //                .pathMatchers(HttpMethod.GET,"/api/properties").permitAll()
    //                .pathMatchers(HttpMethod.GET,"/api/properties/stream").permitAll()
    //                .pathMatchers(HttpMethod.POST,"/api/properties").permitAll()
    //                .pathMatchers("/api/premium/properties/**").access(this::authenticate)
    //                .pathMatchers("/api/expenses/**").access(this::authenticate)
    //                .pathMatchers("/api/tenants/**").access(this::authenticate)
    //                .pathMatchers("/api/user/**").permitAll()
    //                .pathMatchers(HttpMethod.GET, "/api/premium/subscribe/**").hasRole(Roles.ROLE_USER.getRole())
    //                .pathMatchers(HttpMethod.GET, "/api/premium/landlord").hasRole(Roles.ROLE_LANDLORD.getRole())
    //                .pathMatchers(HttpMethod.POST, "/payment/charge").access(this::authenticate)
    //                .pathMatchers(HttpMethod.POST, "/payment/save").access(this::authenticate)

    private static final List<String> PATHS_TO_IGNORE_AUTH;

    static {
        PATHS_TO_IGNORE_AUTH = Arrays.asList(
                                        "/api/properties",
                                        "/api/auth",
                                        "/api/user"
                                );
    }

    private ReactiveMongoOperationsSessionRepository sessionRepository;

    public AuthFilter(ReactiveMongoOperationsSessionRepository sessionRepository) {
        this.sessionRepository = sessionRepository;
    }

    @Override
    public Mono<Void> filter(ServerWebExchange serverWebExchange, WebFilterChain webFilterChain) {
        for (String s : PATHS_TO_IGNORE_AUTH) {
            if (serverWebExchange.getRequest().getPath().pathWithinApplication().value().contains(s)) {
                return webFilterChain.filter(serverWebExchange);
            }
        }

        String session = serverWebExchange.getRequest().getHeaders().get("X-AUTH-TOKEN").get(0);
        MongoSession mongoSession = sessionRepository.findById(session).block();

        if (mongoSession == null) {
            throw new AuthTokenInvalidException("Invalid token");
        }
        return webFilterChain.filter(serverWebExchange);
    }
}
