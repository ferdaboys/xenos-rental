package com.xenos.rental.model.property;

import com.xenos.rental.model.Property;
import lombok.*;

import javax.validation.constraints.NotNull;

@Data
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CreateProperty {

    @NotNull
    private String user;
    private Property property;
}
