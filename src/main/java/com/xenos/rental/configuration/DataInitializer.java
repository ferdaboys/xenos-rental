package com.xenos.rental.configuration;

import com.xenos.rental.model.Expense;
import com.xenos.rental.model.Locator;
import com.xenos.rental.model.Property;
import com.xenos.rental.model.User;
import com.xenos.rental.repository.auth.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Component
@Slf4j
public class DataInitializer {

    private final XenosAccountRepository xenosAccountRepository;
    private final XenosPropertyRepository xenosPropertyRepository;
    private final XenosExpenseRepository xenosExpenseRepository;
    private final XenosPasswordResetTokenRepository xenosPasswordResetTokenRepository;
    private final PasswordEncoder passwordEncoder;
    private final XenosTenantRepository tenantRepository;

    public DataInitializer(XenosAccountRepository xenosAccountRepository, XenosPropertyRepository xenosPropertyRepository,
                           XenosExpenseRepository xenosExpenseRepository, XenosPasswordResetTokenRepository xenosPasswordResetTokenRepository, PasswordEncoder passwordEncoder, XenosTenantRepository tenantRepository) {
        this.xenosAccountRepository = xenosAccountRepository;
        this.xenosPropertyRepository = xenosPropertyRepository;
        this.xenosExpenseRepository = xenosExpenseRepository;
        this.xenosPasswordResetTokenRepository = xenosPasswordResetTokenRepository;
        this.passwordEncoder = passwordEncoder;
        this.tenantRepository = tenantRepository;
    }

    @EventListener(ContextRefreshedEvent.class)
    public void init() {
        initUsers();
        initProperties();
        initPasswordTokens();
    }

    private Property p;

    private void initUsers() {
        log.info("start users initialization...");
        this.xenosAccountRepository
                .deleteAll()
                .thenMany(
                        Flux
                                .just("users", "admin")
                                .flatMap(
                                        username -> {
                                            List<String> roles = "users".equals(username)
                                                    ? Arrays.asList("ROLE_USER")
                                                    : Arrays.asList("ROLE_USER", "ROLE_ADMIN");

                                            User user = User.builder()
                                                    .roles(roles)
                                                    .username(username)
                                                    .isPremium(false)
                                                    .password(passwordEncoder.encode("password"))
                                                    .email(username + "@example.com")
                                                    .build();
                                            return this.xenosAccountRepository.save(user);
                                        }
                                )
                )
                .log()
                .subscribe(
                        null,
                        null,
                        () -> log.info("done users initialization...")
                );
    }

    private void initProperties() {
        log.info("start properties initialization...");
        this.xenosPropertyRepository
                .deleteAll()
                .thenMany(
                        Flux
                                .just("236 Sherwood Ave")
                                .flatMap(
                                        name -> {
                                            User details = this.xenosAccountRepository.findByUsername("users").block();
                                            Property p = Property.builder()
                                                    .title("Beautiful 4 Storey House for rent")
                                                    .price(100.00)
                                                    .locator(Locator.builder()
                                                        .address(name)
                                                        .city("Toronto")
                                                        .country("Canada")
                                                        .postalCode("N5V2A2")
                                                        .province("Ontario")
                                                        .build()
                                                    )
                                                    .rooms(5)
                                                    .user(details)
                                                    .build();
                                            return this.xenosPropertyRepository.save(p);
                                        }
                                )
                )
                .log()
                .subscribe(
                        p -> {
                            this.p = p;
                        },
                        null,
                        () -> {
                            initExpenses();
                            log.info("done properties initialization...");
                        }
                );
    }

    private void initExpenses() {
        log.info("start expenses initialization...");
        this.tenantRepository.deleteAll().subscribe(null, null, () -> {
            System.out.println("Deleted all tenants");
        });
        this.xenosExpenseRepository
                .deleteAll()
                .thenMany(
                        Flux
                            .just("Union Gas")
                            .flatMap(
                                    name -> {
                                        User details = this.xenosAccountRepository.findByUsername("users").block();
                                        Expense e = Expense.builder()
                                                .name(name)
                                                .cost(100.00)
                                                .date(new Date())
                                                .property(p)
                                                .user(details)
                                                .build();
                                        return this.xenosExpenseRepository.save(e);
                                    }
                            )
                )
                .log()
                .subscribe(
                        null,
                        null,
                        () -> log.info("done expenses initialization...")
                );
    }

    private void initPasswordTokens() {
        xenosPasswordResetTokenRepository
                .deleteAll()
                .subscribe(
                        null,
                        null,
                        () -> log.info("done password token initialization")
                );
    }

}
