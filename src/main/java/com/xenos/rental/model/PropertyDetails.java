package com.xenos.rental.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PropertyDetails {

    private List<Tenant> numberOfTenants;
    private Double monthlyRate;
    private Map<String, Double> expenses;


}
