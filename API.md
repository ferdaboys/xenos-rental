**Login**
----
Endpoints that will be exposed to the client to authenticate the user

* **URL**

    {HOSTNAME}/auth/login

* **Method:**
  
  <_The request type_>

  `GET`

* **Success Response:**
  
  <_What should the status code be on success and is there any returned data? This is useful when people need to to know what their callbacks should expect!_>

  * **Code:** 200 <br />
    **Content:** `{       	"roles": [
                  		"ROLE_USER"
                  	],
                  	"name": "user"
                  }`<br/>
    **Headers:** `X-AUTH-TOKEN 61301e57-b665-45df-9c0d-eed59950ed1a`
 
* **Error Response:**

  < If user doesn't exist a 401 is returned >

  * **Code:** 401 UNAUTHORIZED <br />


* **Sample Call:**


    curl 
      --request GET
      --url http://localhost:8080/auth/login
      --header 'authorization: Basic dXNlcjpwYXNzd29yZA==' // this is the user:password Base64 Encoded
      --header 'content-type: application/json'

**Register**
----
* **URL**

    {HOSTNAME}/auth/register

* **Method:**
  
  <_The request type_>

  `POST`
* **Request:**
```json
{
	"username": "jakeojero2",
	"password": "test",
	"email": "jake@jake.com"
}
```
* **Success Response:**
  
  * **Code:** 200 <br />
    **Content:** <br/>
    ```json
    {
           	"id": "5a89c8b4082e31067099aedf",
           	"username": "jakeojero2",
           	"email": "jake@jake.com",
           	"active": true,
           	"roles": [
           		"ROLE_USER"
           	]
    }
    ```
 
* **Error Response:**

  < Username Duplicated >

  * **Code:** 409 CONFLICT <br />
  ```json
  {
  	"status": "CONFLICT",
  	"code": "AUTH.100",
  	"message": "A user with that username already exists"
  }
  ```


* **Sample Call:**


    curl --request POST \
      --url http://localhost:8080/auth/register \
      --header 'content-type: application/json' \
      --data '{
    	"username": "jakeojero2",
    	"password": "test",
    	"email": "jake@jake.com"
    }'
