package com.xenos.rental.model.password;

import lombok.Data;

@Data
public class UpdatePasswordRequest {

    private String id;
    private String password;
}
