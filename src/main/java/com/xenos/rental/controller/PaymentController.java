package com.xenos.rental.controller;

import com.stripe.model.Charge;
import com.xenos.rental.exception.UserNotFoundException;
import com.xenos.rental.model.User;
import com.xenos.rental.repository.auth.XenosAccountRepository;
import com.xenos.rental.service.StripeClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/payment")
public class PaymentController {

    private StripeClient stripeClient;
    private XenosAccountRepository accountRepository;

    @Autowired
    PaymentController(StripeClient stripeClient, XenosAccountRepository accountRepository) {
        this.stripeClient = stripeClient;
        this.accountRepository = accountRepository;
    }

    @PostMapping("/charge")
    public Charge chargeCard(@RequestHeader("token") String token, @RequestHeader("amount") String amount) throws Exception {
        Double amountDouble = Double.parseDouble(amount);
        return this.stripeClient.chargeCreditCard(token, amountDouble);
    }

    @PostMapping("/save")
    public void saveUser(@RequestBody User user) {
        User userToUpdate = accountRepository.findByUsername(user.getUsername()).block();

        if (userToUpdate == null)
            throw new UserNotFoundException("The user could not be found");

        userToUpdate.setIsPremium(user.getIsPremium());
        userToUpdate.setRoles(user.getRoles());

        accountRepository.save(userToUpdate).subscribe();
    }
}
