package com.xenos.rental.repository.auth;

import com.xenos.rental.model.Tenant;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface XenosTenantRepository extends ReactiveMongoRepository<Tenant, String> {
    Mono<Tenant> findById(String id);
    Flux<Tenant> findByPropertyId(final String id);
    Flux<Tenant> findByUserId(final String id);
}
