package com.xenos.rental.exception;

public class ResetTokenInvalidException extends RuntimeException {

    public ResetTokenInvalidException() {
        super();
    }

    public ResetTokenInvalidException(String message) {
        super(message);
    }

    public ResetTokenInvalidException(String message, Throwable cause) {
        super(message, cause);
    }
}
