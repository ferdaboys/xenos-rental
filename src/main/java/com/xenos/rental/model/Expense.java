package com.xenos.rental.model;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Data
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document
public class Expense {

    @Id
    private String id;

    @DBRef
    private Property property;
    @DBRef
    private User user;

    private String name;
    private Date date;
    private double cost;

}
