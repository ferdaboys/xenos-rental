package com.xenos.rental.controller;

import com.xenos.rental.exception.UserNotFoundException;
import com.xenos.rental.model.Property;
import com.xenos.rental.model.Roles;
import com.xenos.rental.model.User;
import com.xenos.rental.repository.auth.XenosAccountRepository;
import com.xenos.rental.repository.auth.XenosPropertyRepository;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/api/premium")
public class PremiumController {

    private XenosPropertyRepository propertyRepository;
    private XenosAccountRepository accountRepository;

    public PremiumController(XenosPropertyRepository propertyRepository, XenosAccountRepository accountRepository) {
        this.propertyRepository = propertyRepository;
        this.accountRepository = accountRepository;
    }

    @GetMapping("/subscribe/{username}")
    public Mono<User> subscribe(@PathVariable String username) {
        // Get user add roles and set is premium
        User user = this.accountRepository.findByUsername(username).block();

        if(user == null)
            throw new UserNotFoundException("Could not find user: " + username);

        user.getRoles().add(Roles.ROLE_LANDLORD.getRole());
        user.setIsPremium(true);

        // Save user
        this.accountRepository.save(user);

        return Mono.just(user);
    }

    @GetMapping("/properties")
    public Flux<Property> getUsersProperties(@RequestParam("username") String username) {
        return propertyRepository.findAllByUser(
                accountRepository.findByUsername(username).block()
        );
    }

}
