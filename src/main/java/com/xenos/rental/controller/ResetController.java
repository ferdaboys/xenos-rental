package com.xenos.rental.controller;

import com.xenos.rental.exception.ResetTokenInvalidException;
import com.xenos.rental.exception.UserNotFoundException;
import com.xenos.rental.model.User;
import com.xenos.rental.model.password.UpdatePasswordRequest;
import com.xenos.rental.repository.auth.XenosAccountRepository;
import com.xenos.rental.service.MailSender;
import com.xenos.rental.service.PasswordResetTokenService;
import org.bson.internal.Base64;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ServerWebExchange;

import java.util.UUID;

@RestController
@RequestMapping("/api")
public class ResetController {

    private MailSender mailSender;
    private XenosAccountRepository accountRepository;
    private PasswordResetTokenService tokenService;
    private PasswordEncoder encoder;
    @Value("${mail.url}")
    private String baseUrl;

    public ResetController(@Qualifier("xenosMail") MailSender mailSender, XenosAccountRepository accountRepository, PasswordResetTokenService tokenService, PasswordEncoder encoder) {
        this.mailSender = mailSender;
        this.accountRepository = accountRepository;
        this.tokenService = tokenService;
        this.encoder = encoder;
    }

    @PostMapping("/user/resetPassword")
    public void resetPassword(ServerWebExchange serverWebExchange, @RequestBody User user) {
        User userToChange = accountRepository.findByUsername(user.getUsername()).block();

        if (userToChange == null)
            throw new UserNotFoundException("That user could not be found");

        String token = UUID.randomUUID().toString();
        tokenService.createToken(userToChange, token);
        createAndSendEmail(token, userToChange);
    }

    @GetMapping("/user/validateResetToken")
    public void validateToken(@RequestParam("token") String token) {
        if (!tokenService.validateToken(token))
            throw new ResetTokenInvalidException("The token was invalid or expired.");
    }

    @PostMapping("/user/updatePassword")
    public void validateToken(@RequestBody UpdatePasswordRequest request) {
        User user = this.accountRepository.findById(request.getId()).block();

        byte[] arr = Base64.decode(request.getPassword());
        String password = new String(arr);
        user.setPassword(encoder.encode(password));
        this.accountRepository.save(user).subscribe();
    }

    private void createAndSendEmail(String token, User userToChange) {
        String url = this.baseUrl + "/user/changePassword/" + userToChange.getId()
                     + "/" + token;
        String message = "Did you request to resert your password?";
        String body = message + "\n\n" + url;
        mailSender.sendMail("admin@jakeojero.me", userToChange.getEmail(), "Xenos Reset Password", body);
    }
}
