package com.xenos.rental.repository.auth;

import com.xenos.rental.model.PasswordResetToken;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Mono;

public interface XenosPasswordResetTokenRepository extends ReactiveMongoRepository<PasswordResetToken, String> {
    Mono<PasswordResetToken> findByToken(String token);
}
