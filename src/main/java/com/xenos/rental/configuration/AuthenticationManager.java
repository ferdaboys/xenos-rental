package com.xenos.rental.configuration;

import com.xenos.rental.exception.ControllerExceptionHandler;
import com.xenos.rental.exception.UserNotFoundException;
import com.xenos.rental.model.User;
import com.xenos.rental.repository.auth.XenosAccountRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.ReactiveAuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.DelegatingPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Slf4j
@Component
public class AuthenticationManager implements ReactiveAuthenticationManager {


    private XenosAccountRepository accountRepository;
    @Autowired
    PasswordEncoder encoder;
    @Autowired
    ControllerExceptionHandler handler;

    @Autowired
    public AuthenticationManager(XenosAccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }


    @Override
    public Mono<Authentication> authenticate(Authentication authentication) {
        User user = accountRepository.findByUsername(authentication.getPrincipal().toString()).block();

        if(user == null)
            throw new UserNotFoundException("User Not Found");
        else if(encoder.matches(authentication.getCredentials().toString(), user.getPassword()))
            authentication = new UsernamePasswordAuthenticationToken(user.getUsername(), user.getPassword(), authentication.getAuthorities());
        else
            throw new BadCredentialsException("Bad Credentials");

        return Mono.just(authentication);
    }
}
