package com.xenos.rental.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Locator {

    private String address;
    private String postalCode;
    private String city;
    private String province;
    private String country;

}
