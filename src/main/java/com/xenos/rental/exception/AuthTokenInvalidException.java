package com.xenos.rental.exception;

public class AuthTokenInvalidException extends RuntimeException {

    public AuthTokenInvalidException() {
        super();
    }

    public AuthTokenInvalidException(String message) {
        super(message);
    }

    public AuthTokenInvalidException(String message, Throwable cause) {
        super(message, cause);
    }
}
