package com.xenos.rental.repository.auth;

import com.xenos.rental.model.Expense;
import com.xenos.rental.model.Property;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface XenosExpenseRepository extends ReactiveMongoRepository<Expense, String> {
    Flux<Expense> findByPropertyId(final String id);
    Flux<Expense> findAllByUserId(String id);
}
