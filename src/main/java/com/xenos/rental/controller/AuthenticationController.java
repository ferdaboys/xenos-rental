package com.xenos.rental.controller;

import com.mongodb.DuplicateKeyException;
import com.xenos.rental.exception.UsernameAlreadyExistsException;
import com.xenos.rental.model.Roles;
import com.xenos.rental.model.User;
import com.xenos.rental.model.register.RegisterUser;
import com.xenos.rental.repository.auth.XenosAccountRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.reactive.function.server.ServerResponse;
import org.springframework.web.server.WebSession;
import reactor.core.publisher.Mono;

import java.security.Principal;
import java.util.HashMap;
import java.util.Map;

@RestController
@Slf4j
@RequestMapping("/api/auth")
public class AuthenticationController {

    // /login
    // /register

    private final XenosAccountRepository repository;
    private final PasswordEncoder passwordEncoder;

    public AuthenticationController(XenosAccountRepository repository, PasswordEncoder passwordEncoder) {
        this.repository = repository;
        this.passwordEncoder = passwordEncoder;
    }

    @GetMapping("/login")
    public Mono<Map> current(@AuthenticationPrincipal Mono<Principal> principal) {
        return principal
                .map( user -> {

                    User userRepo = repository.findByUsername(user.getName()).block();
                    Map<String, Object> map = new HashMap<>();
                    map.put("username", userRepo.getUsername());
                    map.put("email", userRepo.getEmail());
                    map.put("isPremium", userRepo.getIsPremium());
                    map.put("id", userRepo.getId());
                    map.put("roles", userRepo.getRoles());
                    return map;
                });
    }

    @GetMapping("/users/{username}")
    public Mono<User> get(@PathVariable() String username) {
        return this.repository.findByUsername(username);
    }

    @GetMapping("/logout")
    public Mono<Void> logout(WebSession webSession) {
        return webSession.invalidate();
    }

    @PostMapping("/register")
    public Mono<User> register(@RequestBody RegisterUser userToCreate) {

        User user = User.builder()
                        .email(userToCreate.getEmail())
                        .password(passwordEncoder.encode(userToCreate.getPassword()))
                        .username(userToCreate.getUsername())
                        .build();

        user.getRoles().add(Roles.ROLE_USER.getRole());

        return this.repository.save(user);
    }

}
