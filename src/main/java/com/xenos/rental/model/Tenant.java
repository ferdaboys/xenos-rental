package com.xenos.rental.model;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Data
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document
public class Tenant {

    @Id
    private String id;
    @DBRef
    private User user;
    @DBRef
    private Property property;

    private String firstname;
    private String lastname;
    private Double rent;
    private Date date;

}
