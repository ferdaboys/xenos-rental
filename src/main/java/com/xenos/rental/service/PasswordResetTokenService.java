package com.xenos.rental.service;

import com.xenos.rental.model.PasswordResetToken;
import com.xenos.rental.model.User;
import com.xenos.rental.repository.auth.XenosPasswordResetTokenRepository;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Date;

@Service
public class PasswordResetTokenService {

    private XenosPasswordResetTokenRepository passwordResetTokenRepository;

    public PasswordResetTokenService(XenosPasswordResetTokenRepository passwordResetTokenRepository) {
        this.passwordResetTokenRepository = passwordResetTokenRepository;
    }

    public void createToken(User user, String token) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.HOUR_OF_DAY, 12);

        PasswordResetToken resetToken = PasswordResetToken.builder()
                                        .user(user).token(token).expiryDate(calendar.getTime())
                                        .build();
        passwordResetTokenRepository.save(resetToken).subscribe();
    }

    public boolean validateToken(String token) {

        PasswordResetToken resetToken = passwordResetTokenRepository.findByToken(token).block();

        if (resetToken == null)
            return false;

        Calendar calendar = Calendar.getInstance();
        return resetToken.getExpiryDate().getTime() - calendar.getTime().getTime() > 0;
    }

}
