package com.xenos.rental.configuration;

import com.xenos.rental.exception.AuthTokenInvalidException;
import com.xenos.rental.exception.InvalidCredentialsException;
import com.xenos.rental.filter.AuthFilter;
import com.xenos.rental.model.Roles;
import com.xenos.rental.repository.auth.XenosAccountRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authorization.AuthorizationDecision;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.SecurityWebFiltersOrder;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.ReactiveUserDetailsService;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.server.SecurityWebFilterChain;
import org.springframework.security.web.server.authorization.AuthorizationContext;
import org.springframework.security.web.server.context.WebSessionServerSecurityContextRepository;
import org.springframework.session.data.mongo.MongoSession;
import org.springframework.session.data.mongo.ReactiveMongoOperationsSessionRepository;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.reactive.CorsWebFilter;
import org.springframework.web.cors.reactive.UrlBasedCorsConfigurationSource;
import org.springframework.web.util.pattern.PathPatternParser;
import reactor.core.publisher.Mono;

import java.util.Map;

@Configuration
@Slf4j
@EnableWebFluxSecurity
public class SecurityConfig {

    @Autowired
    AuthenticationManager authenticationManager;
    @Autowired
    ReactiveMongoOperationsSessionRepository sessionRepository;
    @Autowired
    AuthFilter authFilter;
    @Bean
    public WebSessionServerSecurityContextRepository webSessionServerSecurityContextRepository() {
        return new WebSessionServerSecurityContextRepository();
    }


    @Bean
    public SecurityWebFilterChain securityWebFilterChain(ServerHttpSecurity http) {

        return http.httpBasic().securityContextRepository(webSessionServerSecurityContextRepository())
                .and()
                .formLogin().disable()
                .csrf().disable()
                .logout().disable()
                .authenticationManager(authenticationManager).addFilterAt(authFilter, SecurityWebFiltersOrder.LAST).authorizeExchange()
                .anyExchange().permitAll()
                .and().build();

    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return PasswordEncoderFactories.createDelegatingPasswordEncoder();
    }

    private Mono<AuthorizationDecision> authenticate(Mono<Authentication> authentication, AuthorizationContext context) {
        return authentication
                .map(a -> a.isAuthenticated())
                .map(granted -> {
                    String session = context.getExchange().getRequest().getHeaders().get("X-AUTH-TOKEN").get(0);
                    MongoSession mongoSession = sessionRepository.findById(session).block();
                   if (granted && mongoSession != null) {
                       return new AuthorizationDecision(granted);
                   }
                   else
                       throw new AuthTokenInvalidException("Failed To authenticate token");
                });
    }


    @Bean
    ReactiveUserDetailsService reactiveUserDetailsService(XenosAccountRepository repository) {
        return (username) -> repository.findByUsername(username)
                                        .map(u -> User.withUsername(u.getUsername())
                                                .password(u.getPassword())
                                                .authorities(u.getRoles().toArray(new String[0]))
                                                .accountExpired(!u.isActive())
                                                .credentialsExpired(!u.isActive())
                                                .disabled(!u.isActive())
                                                .accountLocked(!u.isActive())
                                                .build()
                                        );
    }
}
