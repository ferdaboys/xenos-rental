package com.xenos.rental.model;


public enum Roles {
    ROLE_USER("ROLE_USER"),
    ROLE_ADMIN("ROLE_ADMIN"),
    ROLE_LANDLORD("ROLE_LANDLORD"),
    ROLE_TENNANT("ROLE_TENNANT");

    private String role;

    Roles(String role) {
        this.role = role;
    }

    public String getRole() {
        return role;
    }
}
