package com.xenos.rental.model;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Data
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document
public class PasswordResetToken {

    @Id
    private String id;
    private String token;
    @DBRef
    private User user;

    private Date expiryDate;
}
