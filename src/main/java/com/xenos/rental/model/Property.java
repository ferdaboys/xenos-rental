package com.xenos.rental.model;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;

@Data
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document
public class Property {

    @Id
    private String id;
    @DBRef
    private User user;

    @NotNull
    private String title;
    @NotNull
    private Integer rooms;
    @NotNull
    private Double price;
    @NotNull
    private Locator locator;

    private Boolean promoted = false;

    private ArrayList<String> notes;


}
