package com.xenos.rental.exception;

public class InvalidSearchValueException extends RuntimeException {

    public InvalidSearchValueException() {
        super();
    }

    public InvalidSearchValueException(String message) {
        super(message);
    }

    public InvalidSearchValueException(String message, Throwable cause) {
        super(message, cause);
    }
}
