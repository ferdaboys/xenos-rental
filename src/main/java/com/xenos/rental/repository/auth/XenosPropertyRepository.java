package com.xenos.rental.repository.auth;

import com.xenos.rental.model.Property;
import com.xenos.rental.model.User;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface XenosPropertyRepository extends ReactiveMongoRepository<Property, String> {

    Mono<Property> findById(String id);
    Flux<Property> findByTitle(String title);
    Flux<Property> findAllByLocator_City(String city);
    Flux<Property> findAllByLocator_Province(String province);
    Flux<Property> findAllByRooms(Integer rooms);
    Flux<Property> findAllByPriceGreaterThanEqual(Double price);
    Flux<Property> findAllByPriceLessThanEqual(Double price);
    Flux<Property> findAll();
    Flux<Property> findAllByUser(User user);
}
