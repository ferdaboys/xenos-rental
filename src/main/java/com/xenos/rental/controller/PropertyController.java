package com.xenos.rental.controller;

import com.xenos.rental.exception.InvalidSearchCriteriaException;
import com.xenos.rental.exception.InvalidSearchValueException;
import com.xenos.rental.exception.UserNotFoundException;
import com.xenos.rental.model.Property;
import com.xenos.rental.model.User;
import com.xenos.rental.model.property.CreateProperty;
import com.xenos.rental.repository.auth.XenosAccountRepository;
import com.xenos.rental.repository.auth.XenosPropertyRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;

@RestController
@Slf4j
@RequestMapping("/api/properties")
public class PropertyController {

    private XenosPropertyRepository propertyRepository;
    private XenosAccountRepository accountRepository;

    public PropertyController(XenosPropertyRepository propertyRepository, XenosAccountRepository accountRepository) {
        this.propertyRepository = propertyRepository;
        this.accountRepository = accountRepository;
    }

    @GetMapping("/{search}/{value}")
    public Flux<Property> searchProperties(@PathVariable("search") String search, @PathVariable("value") String value) {

        try {
            switch (search) {
                case "city":
                    return propertyRepository.findAllByLocator_City(value);
                case "province":
                    return propertyRepository.findAllByLocator_Province(value);
                case "pricemorethan":
                    return propertyRepository.findAllByPriceGreaterThanEqual(Double.parseDouble(value));
                case "pricelessthan":
                    return propertyRepository.findAllByPriceLessThanEqual(Double.parseDouble(value));
                case "rooms":
                    return propertyRepository.findAllByRooms(Integer.valueOf(value));
                case "id":
                    return Flux.just(propertyRepository.findById(value).block());
                case "title":
                    return propertyRepository.findByTitle(value);
                default:
                    throw new InvalidSearchCriteriaException("Cannot search for " + search);
            }
        }
        catch (NumberFormatException ex) {
            throw new InvalidSearchValueException("Invalid value for that operation :: Value " + value);
        }

    }

    @PostMapping("")
    public Mono<Property> createProperty(@RequestBody @Valid CreateProperty property) {
        log.info("User = " + property.getUser());
        Property prop = property.getProperty();
        log.info("Adding user to the property");
        User user = this.accountRepository.findByUsername(property.getUser()).block();

        if(user == null)
            throw new UserNotFoundException("User not found");

        prop.setUser(user);
        return this.propertyRepository.save(prop);
    }

    @GetMapping(value = "")
    public Flux<Property> getAllProperties() {
        return this.propertyRepository.findAll();
    }

    @GetMapping(value = "/stream", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<Property> propertyStream() {
        return this.propertyRepository.findAll();
    }

    @PutMapping("")
    public Mono<Property> update(@RequestBody @Valid Property property) {
        return this.propertyRepository.save(property);
    }
}
