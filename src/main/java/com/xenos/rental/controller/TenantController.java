package com.xenos.rental.controller;

import com.xenos.rental.model.Expense;
import com.xenos.rental.model.Tenant;
import com.xenos.rental.repository.auth.XenosExpenseRepository;
import com.xenos.rental.repository.auth.XenosTenantRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Date;

@RestController
@Slf4j
@RequestMapping("/api")
public class TenantController {

    private final XenosTenantRepository repository;
    public TenantController(XenosTenantRepository repository) {
        this.repository = repository;
    }

    @GetMapping("/tenants/{propertyId}")
    public Flux<Tenant> get (@PathVariable String propertyId) {
        return this.repository.findByPropertyId(propertyId);
    }
    @GetMapping("/tenants/user/{userId}")
    public Flux<Tenant> getTenants (@PathVariable String userId) {
        return this.repository.findByUserId(userId);
    }

    @DeleteMapping("/tenants/{tenantId}")
    public void deleteTenant (@PathVariable String tenantId) {
        Tenant t = this.repository.findById(tenantId).block();
        this.repository.delete(t).subscribe();
    }

    @PostMapping("/tenants/save")
    public Mono<Tenant> save(@RequestBody Tenant tenant) {
        tenant.setDate(new Date());
        return this.repository.save(tenant);
    }


}
